import React from 'react'

export default class TodoInput extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      text: '',
      error: ''
    }
  }

  todoAdd = () => {
    const { text } = this.state;
    if (text) {
      this.props.todoAdd(this.state.text);
      this.setState({text: ''})
    } else {
      this.setState({error: 'Нельзя добавить тудушку с пустым текстом.'})
    }
  }

  render() {
    const { text, error } = this.state
    return (
      <div className="todo__input-wrapper">
        { error &&
          <div>
            {error}
          </div>
        }
        <input type="text"
               onChange={(e) => this.setState({text: e.target.value, error: ''})}
               value={this.state.text}
               className="todo__input"
        />
        <button onClick={this.todoAdd} className="todo__input-button" disabled={!text}>
          Add to-do!
        </button>
      </div>
    )
  }
}