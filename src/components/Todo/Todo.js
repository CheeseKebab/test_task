import React from 'react'
import TodoInput from './TodoInput'
import cn from 'classnames'

export default class ToDo extends React.Component {
  constructor(props){
    super(props);
  }

  todoAdd(text) {
    this.props.addTodo(text);
  };

  render() {
    const { todos, filter } = this.props;

    return(
      <div className="todo__wrapper">
        <div className="todo__header">TODOS!</div>
        <div className="todo__filter">
          <button onClick={() => this.props.filterTodo('new')}
                  className="todo__input-button"
                  style={{marginLeft: 0}}>
            Do this!
          </button>
          <button onClick={() => this.props.filterTodo('done')} className="todo__input-button">
            Done!
          </button>
          <button onClick={() => this.props.filterTodo('all')} className="todo__input-button">
            All!
          </button>
        </div>
        <div className="todo__inner">
          {todos.map((todo, i) => {
            if (todo.status === filter || filter === 'all')
              return (
                <div key={i} className={cn("todo__item", {"done": todo.status === 'done'})}>
                  {todo.text}
                  {todo.status === 'new' &&
                    <div className="todo__item-del" onClick={this.props.delTodo.bind(this, i)}>X</div>
                  }
                </div>
            )
          })}
        </div>
        <TodoInput todoAdd={this.todoAdd.bind(this)}/>
      </div>
    )
  }
}