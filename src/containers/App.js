import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import ToDo from '../components/Todo/Todo'
import * as todoActions from '../actions/TodoActions'

class App extends React.Component {
  constructor(props){
    super(props)
  }

  render() {
    const { todos, filter } = this.props
    const { addTodo, delTodo, filterTodo } = this.props.todoActions

    return(
      <div>
        <ToDo todos={todos} filter={filter} addTodo={addTodo} delTodo={delTodo} filterTodo={filterTodo}/>
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    todoActions: bindActionCreators(todoActions, dispatch)
  }
}

function mapStateToProps(state) {
  console.log(state.todo)
  return {
    todos: state.todo.todos,
    filter: state.todo.filter
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)