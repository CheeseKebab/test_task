import { ADD_TODO, DEL_TODO, FILTER_TODO } from '../constants/Todo'

export function addTodo(text) {
  return {
    type: ADD_TODO,
    payload: text
  }
}

export function delTodo(i) {
  return {
    type: DEL_TODO,
    payload: i
  }
}

export function filterTodo(filter) {
  return {
    type: FILTER_TODO,
    payload: filter
  }
}