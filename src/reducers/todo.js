import { ADD_TODO, DEL_TODO, FILTER_TODO } from '../constants/Todo'

const initialState = {
  todos: [{text: 'first task', status: 'new'}, {text: 'second task', status: 'new'}],
  filter: 'new'
}

export default function todo(state = initialState, action) {
  switch (action.type) {
    case ADD_TODO:
      return { ...state, todos: [...state.todos, { text: action.payload, status: 'new'}] }

    case DEL_TODO:
      //state.todos.splice(action.payload, 1)
      let newState = state.todos
      newState[action.payload].status = 'done'
      return { ...state, todos: [...newState] }

    case FILTER_TODO:
      return { ...state, filter: action.payload }

    default:
      return state;
  }
}